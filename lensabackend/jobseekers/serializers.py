from rest_framework import serializers

from .models import Talent, JobSeeker


class TalentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Talent


class JobSeekerSerializer(serializers.ModelSerializer):
    talents = TalentSerializer(many=True)

    class Meta:
        model = JobSeeker
