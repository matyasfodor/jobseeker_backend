from __future__ import unicode_literals

from django.db import models


class Talent(models.Model):
    name = models.CharField(max_length=20, unique=True)


class JobSeeker(models.Model):
    GENDER_MALE = 'M'
    GENDER_FEMALE = 'F'
    GENDER_CHOICES = (
        (GENDER_MALE, 'male'),
        (GENDER_FEMALE, 'female')
    )

    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    date_of_birth = models.DateField()
    talents = models.ManyToManyField(Talent, related_name='talent')
