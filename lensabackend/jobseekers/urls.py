from .views import JobSeekerViewSet, TalentViewSet

# job_seeker_list = JobSeekerViewSet.as_view({'get': 'list'})
job_seeker_list = JobSeekerViewSet.as_view()

talent_list = TalentViewSet.as_view()
