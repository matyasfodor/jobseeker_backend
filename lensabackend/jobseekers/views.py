from datetime import date, datetime

from rest_framework import (viewsets, filters, generics)
from rest_framework.pagination import LimitOffsetPagination

from .models import Talent, JobSeeker
from .serializers import JobSeekerSerializer, TalentSerializer


def yearsago(years):
    from_date = datetime.now()
    return from_date.replace(year=from_date.year - years)


class TalentViewSet(generics.ListAPIView):
    """API endpoint for listing users."""

    pagination_class = None
    serializer_class = TalentSerializer
    queryset = Talent.objects.all()


class JobSeekerViewSet(generics.ListAPIView):
    """API endpoint for listing users."""

    pagination_class = LimitOffsetPagination
    model = JobSeeker
    serializer_class = JobSeekerSerializer

    def get_queryset(self):
        filter_parameters = {}
        if 'gender' in self.request.query_params:
            filter_parameters['gender'] = self.request.query_params['gender']

        talents = self.request.query_params.getlist('talents')
        if len(talents):
            filter_parameters['talents__in'] = [int(talent_id) for talent_id in talents]

        if 'age_from' in self.request.query_params:
            filter_parameters['date_of_birth__lt'] = yearsago(int(self.request.query_params['age_from']))

        if 'age_to' in self.request.query_params:
            filter_parameters['date_of_birth__gt'] = yearsago(int(self.request.query_params['age_to']))

        query_set = JobSeeker.objects.filter(**filter_parameters)

        if 'order_by' in self.request.query_params:
            return query_set.order_by(self.request.query_params['order_by'])

        return query_set
