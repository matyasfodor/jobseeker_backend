# Installation

prerequisites: 
 - virtualenv
 - pip
  
    cd $WORKSPACE
    git clone ...
    cd lensa_backend
  
    virtualenv virtualenv
    pip install -r requirements.txt
    
# DB fixture

The DB is added to the repo for the sake of simplicity, which is not a good practice, but there's no confidential data in it.
So it should work out of the box.


# starting the webserver

    cd $WORKSPACE/lensa_backend/lensabackend
    # you might need to run
    python manage.py migrate
    
    python manage.py runserver

The frontend uses `localhost:8000`, the default django port, this can be configured.

The code is not considered production ready, I don't utilize the advanced filtering/ordering capabilities of django rest framework, and also security was not taken into consideration